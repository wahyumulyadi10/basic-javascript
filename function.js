// Primitive funct
function processData() {
    return this.num1 + this.num2
}
function processDataArg(a,b){
    return a+b
}
// Anonymous funct
const proccesDataAnonym = function () {
    return 80+90
}

// Arrow funct
const processDataArrow =()=>{
    return 90+90
}
const obj={
    num1:20,
    num2:50,
    calculate:processData
}
console.log(obj.calculate())
// console.log(processDataArg(1,2))