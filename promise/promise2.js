const greet = new Promise((resolve, reject)=>{
    // reject('Error ges')
    setTimeout(() => {
        // console.log('After 1 Second')
        resolve('After 1Sec')
    }, 1000)
}
)

async function processAsync(){
    const result = await greet
    console.log(result)
}

processAsync()