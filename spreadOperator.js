// Spread Opt
const arr = [1,2,3]
const arr2 = [4,5,6]

const arr3 = [...arr,...arr2]




const obj1={
    firstName:"Wahyu",
    lastName:"mulyadi",
    fullName:function () {
        return `${this.firstName} ${this.lastName}`
    }
}
const obj2={
    age:23,
    birthDay:"Kapan?"
}
const obj3={...obj1,...obj2}

// output
console.log(obj3)
console.log(arr3)
console.log(obj3.fullName())

