const fetch = require('node-fetch')

async function getData(){

    try {
        // const ress = await fetch('https://jsonplaceholder.typico.com/users')
        const ress = await fetch('https://jsonplaceholder.typicode.com/users')
        const data = await ress.json()
        // console.log(data)
        for (let i = 0; i < data.length; i++) {
            console.log(data[i].name)
            
        }
        
    } catch (error) {
        console.log(error.message)
    }
  
}
getData()